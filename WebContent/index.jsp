<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>WEB应用专业团队</title>
        <link rel="Shortcut Icon" href="images/web.ico" type="image/x-icon" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/templatemo-misc.css">
        <link rel="stylesheet" href="css/templatemo-style.css">
        <link rel="stylesheet" href="css/swiper-3.4.2.min.css" />
        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
</head>
<body>

 <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background-image: url(images/slide1.jpg);">
                    <div class="overlay-s"></div>
                    <div class="slider-caption">
                        <div class="inner-content">
                            <h2>新生代力量</h2>
                            <p>We are Young!</p>
                            <a href="members.jsp" class="main-btn white">View Members</a>                          
                        </div> <!-- /.inner-content -->
                    </div> <!-- /.slider-caption -->
                </div> <!-- /.swier-slide -->

                <div class="swiper-slide" style="background-image: url(images/slide2.jpg);">
                    <div class="overlay-s"></div>
                    <div class="slider-caption">
                        <div class="inner-content">
                            <h2>未来科技界大佬</h2>
                            <p>We come with new fresh and unique ideas.</p>
                            <a href="members.jsp" class="main-btn white">View Members</a>
                        </div> <!-- /.inner-content -->
                    </div> <!-- /.slider-caption -->
                </div> <!-- /.swier-slide -->

                <div class="swiper-slide" style="background-image: url(images/slide3.jpg);">
                    <div class="overlay-s"></div>
                    <div class="slider-caption">
                        <div class="inner-content">
                            <h2>We Are Coming!</h2>
                            <p>We are a team and we work as a team.</p>
                            <a href="members.jsp" class="main-btn white">View Members</a>                            
                        </div> <!-- /.inner-content -->
                    </div> <!-- /.slider-caption -->
                </div> <!-- /.swier-slide -->
				<div class="swiper-button-prev"></div>
	    		<div class="swiper-button-next"></div>
	            <div class="swiper-pagination"></div>
	            <div class="swiper-scrollbar"></div>
            </div> <!-- /.swiper-wrapper -->            
        </div> <!-- /.swiper-container -->

        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/swiper-3.4.2.jquery.min.js" ></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Preloader -->
        <script type="text/javascript">
            //<![CDATA[
            $(window).load(function() { // makes sure the whole site is loaded
                $('.loader-item').fadeOut(); // will first fade out the loading animation
                    $('#pageloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow-y':'visible'});
            })
            //]]>
            var mySwiper = new Swiper('.swiper-container',{
            	 direction:'horizontal',
           		 autoplay:true,           		 
           		 speed:8000,
           		 loop:true           
    		})
        </script>
</body>
</html>