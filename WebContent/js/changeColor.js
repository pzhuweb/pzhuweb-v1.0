(function(){
	//声明静态变量，字体颜色数组和背景颜色数组以及改变颜色对象
	var ARRAY_FONT_COLOR = ['#FCC','#F6F','#CFF','#9FF','#9FF','#6F0','#3FF','#FF6'],
		ARRAY_BACKGROUND_COLOR = ['#FC0','#F60','#CF0','#9F0','#6FF','#3F0','#00F','#CCC'],
		ChangeColor = function(){};

	ChangeColor.prototype={
		change_FC:function(i,obj){
			obj.css({
				"color":ARRAY_BACKGROUND_COLOR[i],
				"transition":"all "+Math.random()+"s linear"
			});
			return this;
		},
		change_BC:function(i,obj){
			obj.css("text-shadow","5px 5px 10px "+ARRAY_FONT_COLOR[i]);
			return this;
		}
	};
	//添加一个定时器
	var SI = setInterval(function(){

		var length = ARRAY_FONT_COLOR.length,
			changeObj = new ChangeColor(),
			$font = $('.team-title'),
			randomN = 0,
			randomO = 0;

			randomN = Math.floor(Math.random()*length);
			randomN = randomN == randomO?(randomN+=1<8?randomN+=1:0):randomN;
			
			changeObj.change_FC(randomN,$font).change_BC(randomN,$font);
			randomO = randomN;
			
	},600);
})();
