<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>WEB应用专业团队</title>
        <link rel="Shortcut Icon" href="images/web.ico" type="image/x-icon" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/templatemo-misc.css">
        <link rel="stylesheet" href="css/templatemo-style.css">
        <link rel="stylesheet" href="css/swiper-3.4.2.min.css" />
        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
</head>
<body>
 <section id="pageloader">
            <div class="loader-item fa fa-spin colored-border"></div>
        </section> <!-- /#pageloader -->          

        <header class="site-header container-fluid">
            <div class="top-header">
                <div class="logo col-md-6 col-sm-6">
                	<h1><a href="index.jsp"><em>Web</em>应用专业团队</a></h1>
                    <span>一切以代码为基础，行走在潮流之间！</span>
                </div> <!-- /.logo -->
                <div class="social-top col-md-6 col-sm-6">
                    <ul>                      
                        <li>报名加入团队<a href="regist/index.html" class="fa fa-user"></a></li>
                        <li>前端在线学习 <a href="http://218.6.132.18/meol/jpk/course/layout/lesson/index.jsp?courseId=42728" class="fa fa-plus"></a></li>   
                        <li>后台在线学习<a href="http://218.6.132.18/meol/jpk/course/layout/lesson/index.jsp?courseId=42657" class="fa fa-sign-in"></a></li>
                    </ul>
                </div> <!-- /.social-top -->
            </div> <!-- /.top-header -->
            <div class="main-header">
                <div class="row">                    
                    <div class="menu-wrapper">
                        <a href="#" class="toggle-menu visible-sm visible-xs"><i class="fa fa-bars"></i></a>
                        <ul class="sf-menu hidden-xs hidden-sm">
                            <li class="active"><a href="index.jsp">HOME</a></li>
                            <li><a href="news.jsp">NEWS</a></li>
                            <li><a href="members.jsp">MEMBERS</a></li>
                            <li><a href="demon.jsp">PROJECTS</a></li>
                            <li><a href="#">COMING...</a>
                                <ul>
									<li><a href="regist/index.html">Join us</a></li>
                                    <li><a href="our-team.html">Our Team</a></li>
                                    <li><a href="archives.html">Archives</a></li>
                                    <li><a href="grids.html">Columns</a></li>
                                    <li><a href="404.html">404 Page</a></li>
                                </ul>
                            </li>
                            <li><a href="contact.jsp">CONTACT</a></li>
                        </ul>
                    </div> <!-- /.menu-wrapper -->
                </div> <!-- /.row -->
            </div> <!-- /.main-header -->
            <div id="responsive-menu">
                <ul>
                    <li><a href="index.jsp">HOME</a></li>
                    <li><a href="news.jsp">NEWS</a></li>
                    <li><a href="members.jsp">MEMBERS</a></li>
                    <li><a href="demon.jsp">PROJECTS</a></li>
                    <li><a href="#">COMING...</a>
                        <ul>
							<li><a href="regist/index.html">Join us</a></li>
                            <li><a href="our-team.html">Our Team</a></li>
                            <li><a href="archives.html">Archives</a></li>
                            <li><a href="grids.html">Columns</a></li>
                            <li><a href="404.html">404 Page</a></li>
                        </ul>
                    </li>
                    <li><a href="contact.jsp">CONTACT</a></li>
                </ul>
            </div>
        </header> <!-- /.site-header -->
        
</body>
</html>