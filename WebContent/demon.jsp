<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>WEB应用专业团队</title>
        <link rel="Shortcut Icon" href="images/web.ico" type="image/x-icon" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/templatemo-misc.css">
        <link rel="stylesheet" href="css/templatemo-style.css">
        <link rel="stylesheet" href="css/swiper-3.4.2.min.css" />
        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
</head>
<body>
<div class="content-wrapper">
            <div class="inner-container container">
                <div class="row">
                    <div class="section-header col-md-12">
                        <h2>Our demon</h2>
                        <span>Subtitle Goes Here</span>
                    </div> <!-- /.section-header -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="blog-masonry masonry-true">
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/message.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.pzhuweb.cn/message" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.pzhuweb.cn/message">留言板</a></h2>
                                    <span class="blog-meta">10 April 2017 by LanQuanxiang</span>
                                    <p>留言板是所有用户都可以使用的小程序，属于一个自定义用户名即可进入留言。但是留言板是基于Application存储的，重启服务器之后数据丢失.</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/stu.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.pzhuweb.cn/stu" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.pzhuweb.cn/stu">幸运大抽奖</a></h2>
                                    <span class="blog-meta">8 May 2017 by LanQuanxiang</span>
                                    <p>幸运抽奖是基于Ajax进行开发的，导入数据之后能够进行随机选取。点击页面的任意位置可以开始随机滚动，再次点击页面任意位置暂停滚动。</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/file.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.pzhuweb.cn/file/" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.pzhuweb.cn/file/">文件分享</a></h2>
                                    <span class="blog-meta">6 June 2017 by LanQuanxiang</span>
                                    <p>文件共享是利用第三方组建uploadSmart开发的，可以实现文件的上传和下载.程序的所有信息没有写入数据库，是基于Application内置对象进行存储的，重启服务器之后数据丢失。</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/music.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.pzhuweb.cn/music" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.pzhuweb.cn/music">音乐播放器</a></h2>
                                    <span class="blog-meta">14 May 2017 by LanQuanxiang</span>
                                    <p>音乐播放器是一个前端开发的小程序，在程序中使用了Ajax、HTML5相关知识，利用正则表达式匹配歌词，属于一个前端综合技术编写的程序。播放器实现了顺序播放、随机播放、点选播放等。</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/crm.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.pzhuweb.cn/spring_crm" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.pzhuweb.cn/spring_crm">人事管理</a></h2>
                                    <span class="blog-meta">2 May 2017 by XieWeiming</span>
                                    <p>人事管理系统采用了Struts2, Hibernate, Spring和Ajax。系统中包含文件的上传下载，Ajax中的二级联动异步请求，是一个非常适合于刚学完ssh框架的学生练手的系统。系统页面布局经典简单后台思维模式清晰明了</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/blog-pxf.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.neverlove.me/" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.neverlove.me/">庞晓峰博客</h2>
                                    <span class="blog-meta">31 June 2017 by PangXiaofeng</span>
                                    <p>庞晓峰自己搭建的一个个人博客，可以用来记录一些自己的学习心得和体会。</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/blog.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://119.29.20.79:8080/show/timeaxis.html" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://119.29.20.79:8080/show/timeaxis.html">李宇个人博客</a></h2>
                                    <span class="blog-meta">4 February by LiYu</span>
                                    <p>李宇的个人博客，记录了一些简单的学习心得，同时也对一些精彩的文章进行推荐和转载。前端页面制作了很炫酷的鼠标特效！</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/jd.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.pzhuweb.cn/jd" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.pzhuweb.cn/jd">仿写京东</a></h2>
                                    <span class="blog-meta">4 August 2017 by YuChenbin</span>
                                    <p>仿写的京东首页。</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                        <div class="post-masonry col-md-4 col-sm-6">
                            <div class="blog-thumb">
                                <img src="images/demon/book.jpg" alt="">
                                <div class="overlay-b">
                                    <div class="overlay-inner">
                                        <a href="http://www.pzhuweb.cn/product" class="fa fa-link"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-body">
                                <div class="box-content">
                                    <h2 class="post-title"><a href="http://www.pzhuweb.cn/product">BookStore</a></h2>
                                    <span class="blog-meta">14 June 2017 by ChenBo</span>
                                    <p>网上书城采用了servlet,ajax技术一个简单常见的系统。适合于初学者检验自身能力的系统</p>
                                </div>
                            </div>
                        </div> <!-- /.post-masonry -->
                    </div> <!-- /.blog-masonry -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="pagination text-center">
                            <ul>
                                <li><a href="javascript:void(0)">1</a></li>
                                <li><a href="javascript:void(0)" class="active">2</a></li>
                                <li><a href="javascript:void(0)">3</a></li>
                                <li><a href="javascript:void(0)">4</a></li>
                                <li><a href="javascript:void(0)">...</a></li>
                                <li><a href="javascript:void(0)">11</a></li>
                            </ul>
                        </div> <!-- /.pagination -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
            </div> <!-- /.inner-content -->
        </div> <!-- /.content-wrapper -->

        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Preloader -->
        <script type="text/javascript">
            //<![CDATA[
            $(window).load(function() { // makes sure the whole site is loaded
                $('.loader-item').fadeOut(); // will first fade out the loading animation
                    $('#pageloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow-y':'visible'});
            })
            //]]>
        </script>
</body>
</html>