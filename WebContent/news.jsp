<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>WEB应用专业团队</title>
        <link rel="Shortcut Icon" href="images/web.ico" type="image/x-icon" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/templatemo-misc.css">
        <link rel="stylesheet" href="css/templatemo-style.css">
        <link rel="stylesheet" href="css/swiper-3.4.2.min.css" />
        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
</head>
<body>
	<div class="content-wrapper">
            <div class="inner-container container">
                <div class="row">
                    <div class="section-header col-md-12">
                        <h2>Our News</h2>
                        <span>We've Working Here All The Time.</span>
                    </div> <!-- /.section-header -->
                </div> <!-- /.row -->
                <div class="row">
                    <div class="col-md-6 service-item service-left">
                        <div class="box-content">
                            <div class="service-icon">
                                <i class="li_lab"></i>
                            </div>
                            <div class="service-content">
                                <h4><a href="http://jsj.pzhu.cn/zylm/b631cb5178e747f396acdd2a7ea482a2.shtml">WEB应用专业社团召开新学期指导例会</a></h4>
                                <p>WEB应用专业社团在2017年9月3日星期天晚上19:40在分析测试中心创新实验室7A-3召开了新学期第一次见面会。</p>
                            </div>
                        </div> <!-- /.box-content -->
                    </div> <!-- /.service-item -->
                    <div class="col-md-6 service-item service-right">
                        <div class="box-content">
                            <div class="service-icon">
                                <i class="li_bulb"></i>
                            </div>
                            <div class="service-content">
                                <h4><a href="http://jsj.pzhu.cn/zylm/9f723d2e7d784f02a5545eb8a372eb4d.shtml">WEB应用专业社团启动社团官方网站建设项目</a></h4>
                                <p>WEB应用专业社团在2017年9月10日星期天晚上19:40在分析测试中心创新实验室7A-3对WEB应用专业团队官方网站进行了需求分析和讨论。</p> </div>
                        </div> <!-- /.box-content -->
                    </div> <!-- /.service-item -->
                    <div class="col-md-6 service-item service-left">
                        <div class="box-content">
                            <div class="service-icon">
                                <i class="li_cup"></i>
                            </div>
                            <div class="service-content">
                                <h4><a href="#" >我们还在继续……</a></h4>
                                <p>我们还在努力的制造新闻！</p>
                            </div>
                        </div> <!-- /.box-content -->
                    </div> <!-- /.service-item -->
                    <div class="col-md-6 service-item service-right">
                        <div class="box-content">
                            <div class="service-icon">
                                <i class="li_clock"></i>
                            </div>
                            <div class="service-content">
                                <h4><a href="#" >我们还在继续……</a></h4>
                                <p>我们还在努力的制造新闻！</p>
                            </div>
                        </div> <!-- /.box-content -->
                    </div> <!-- /.service-item -->
                    <div class="col-md-6 service-item service-left">
                        <div class="box-content">
                            <div class="service-icon">
                                <i class="li_like"></i>
                            </div>
                            <div class="service-content">
                                <h4><a href="#" >我们还在继续……</a></h4>
                                <p>我们还在努力的制造新闻！</p>
                           </div>
                        </div> <!-- /.box-content -->
                    </div> <!-- /.service-item -->
                    <div class="col-md-6 service-item service-right">
                        <div class="box-content">
                            <div class="service-icon">
                                <i class="li_study"></i>
                            </div>
                            <div class="service-content">
                                <h4><a href="#" >我们还在继续……</a></h4>
                                <p>我们还在努力的制造新闻！</p>
                           </div>
                        </div> <!-- /.box-content -->
                    </div> <!-- /.service-item -->
                </div> <!-- /.row -->
            </div> <!-- /.inner-content -->
        </div> <!-- /.content-wrapper -->

        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Preloader -->
        <script type="text/javascript">
            //<![CDATA[
            $(window).load(function() { // makes sure the whole site is loaded
                $('.loader-item').fadeOut(); // will first fade out the loading animation
                    $('#pageloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
                $('body').delay(350).css({'overflow-y':'visible'});
            })
            //]]>
        </script>
</body>
</html>