<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="head.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>WEB应用专业团队</title>
        <link rel="Shortcut Icon" href="images/web.ico" type="image/x-icon" />
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/templatemo-misc.css">
        <link rel="stylesheet" href="css/templatemo-style.css">
        <link rel="stylesheet" href="css/swiper-3.4.2.min.css" />
        <script src="js/vendor/modernizr-2.6.1-respond-1.1.0.min.js"></script>
</head>
<body>
<div class="content-wrapper">
            <div class="inner-container container">
                <div class="row">
                    <div class="section-header col-md-12">
                        <h2>MEMBERS</h2>
                        <span>We are here to show</span>
                    </div> <!-- /.section-header -->
                </div> <!-- /.row -->
                <div class="projects-holder-2 row">
                    <div class="filter-categories col-md-2 col-sm-3">
                        <ul class="project-filter">
                            <li class="filter" data-filter="all"><span>All</span></li>
                            <li class="filter" data-filter="front-end"><span>Front-End</span></li>
                            <li class="filter" data-filter="back-end"><span>Back-End</span></li>
                            <li class="filter" data-filter="basics"><span>Basics</span></li>
                            <li class="filter" data-filter="teachers"><span>Teachers</span></li>
                        </ul>
                    </div>
                    <div class="projects-holder col-md-10 col-sm-9">
                        <div class="row">
                            <div class="col-md-6 col-sm-6 project-item mix teachers">
                                <div class="project-thumb">
                                    <img src="images/members/lqx.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/lqx.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">兰全祥</a></h2>
                                    <p>数学与计算机学院教师</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix front-end basics">
                                <div class="project-thumb">
                                    <img src="images/members/lx.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/lx.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">刘星</a></h2>
                                    <p>2015级软件工程1班</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix back-end basics">
                                <div class="project-thumb">
                                    <img src="images/members/cb.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/cb.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">陈波</a></h2>
                                    <p>2015级网络工程2班</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix front-end">
                                <div class="project-thumb">
                                    <img src="images/members/cmm.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/cmm.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">陈苗苗</a></h2>
                                    <p>2014级软件工程1班</p>
                                </div>
                            </div> <!-- /.project-item -->
                            
                            <div class="col-md-6 col-sm-6 project-item mix back-end">
                                <div class="project-thumb">
                                    <img src="images/members/ljx.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/ljx.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">兰家幸</a></h2>
                                    <p>2014级网络工程</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix front-end">
                                <div class="project-thumb">
                                    <img src="images/members/lw.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/lw.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">梁伟</a></h2>
                                    <p>2014级软件工程</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix back-end">
                                <div class="project-thumb">
                                    <img src="images/members/ly.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/ly.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">李宇</a></h2>
                                    <p>2014级软件工程</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix basics">
                                <div class="project-thumb">
                                    <img src="images/members/myl.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/myl.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">马乙黎</a></h2>
                                    <p>2016级计算机科学与技术</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix front-end">
                                <div class="project-thumb">
                                    <img src="images/members/wzd.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/wzd.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">吴振东</a></h2>
                                    <p>2015级软件工程</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix back-end">
                                <div class="project-thumb">
                                    <img src="images/members/xwm.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/xwm.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">谢伟明</a></h2>
                                    <p>2014级软件工程</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix basics">
                                <div class="project-thumb">
                                    <img src="images/members/ytf.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/ytf.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">杨廷发</a></h2>
                                    <p>2016级软件工程1班</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix basics">

                                <div class="project-thumb">
                                    <img src="images/members/yty.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/yty.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">姚添译</a></h2>
                                    <p>2016级软件工程3班</p>
                                </div>
                            </div> <!-- /.project-item -->
                            <div class="col-md-6 col-sm-6 project-item mix back-end basics">
                                <div class="project-thumb">
                                    <img src="images/members/dcy.jpg" alt="">
                                    <div class="overlay-b">
                                        <div class="overlay-inner">
                                            <a href="images/members/dcy.jpg" class="fancybox fa fa-expand" title="Project Title Here"></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-content project-detail">
                                    <h2><a href="project-details.html">丁川芸</a></h2>
                                    <p>2016级软件工程3班</p>
                                </div>
                            </div> <!-- /.project-item -->
                        </div> <!-- /.row -->
                        <div class="load-more">
                            <a href="javascript:void(0)" class="load-more">Load More</a>
                        </div>
                    </div> <!-- /.projects-holder -->
                </div> <!-- /.projects-holder-2 -->
            </div> <!-- /.inner-content -->
        </div> <!-- /.content-wrapper -->

        <script src="js/vendor/jquery-1.11.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Preloader -->
        <script type="text/javascript">
            //<![CDATA[
            $(window).load(function() { 
                $('.loader-item').fadeOut(); 
                    $('#pageloader').delay(350).fadeOut('slow');
                $('body').delay(350).css({'overflow-y':'visible'});
            })
            //]]>
        </script>
</body>
</html>